#!/usr/bin/env python3

"""This is a crude script to try to find all the figs necessary to compile
the latex. It copies them from their source directory directly into this
directory --- e.g., it won't make any subdirectories (most journals don't
allow them). It also changes all the names in the latex source to match the
lack of subdirectories, making a new file called "manuscript.tex" with
these names. If it encounters a duplicate filename, it should crash.

Instructions:
    1. Create a new folder for the standalone. Copy the .tex and this
    script into that folder.
    2. Edit the sourcefile line below.
    3. Run pdflatex on the .tex file once in the source file to generate
    the .log file (where this script looks for figure names).
    4. Run this script.

    5. Copy any bib files, etc., in so it compiles.

Some journals (JCP?) require figures to be in eps format. To enable all PDFs to
be converted over, set convert_to_eps to True.
"""


import os
import shutil

sourcefile = 'v14-aap'
convert_to_eps = False

# Get figure file names.
with open('.'.join([sourcefile, 'log']), 'r') as f:
    lines = f.readlines()

pathnames = []
for _, line in enumerate(lines):
    if line.startswith('LaTeX Warning: File'):
        line = line.strip('\n')
        if "'" not in line:
            # This line must wrap into next.
            line2 = lines[_ + 1]
            line += line2
        pathname = line.split('`')[1]
        pathname = pathname.split("'")[0]
        print(pathname)
        pathnames.append(pathname)


# Copy them, hoping they all have unique names.
filenames = []  # For checking for duplicates
for pathname in pathnames:
    index = 0  # For duplicates
    filename = os.path.split(pathname)[-1]
    print(filename)
    while filename in filenames:
        print('{} already used.'.format(filename))
        if index == 0:
            oldfilename = os.path.splitext(filename)
        filename = '{}-{:d}{}'.format(oldfilename[0], index,
                                      oldfilename[1])
        index += 1
        print('  Trying {}.'.format(filename))
        if filename not in filenames:
            index = 0
    filenames.append(filename)
    shutil.copy(os.path.join(os.pardir, pathname), filename)

# Convert to eps, if asked to.
if convert_to_eps:
    newfilenames = []
    for filename in filenames:
        command = 'pdftops -eps {:s}'.format(filename)
        print(command)
        os.system(command)
        os.remove(filename)
        newfilenames.append(filename[:-3] + 'eps')
    filenames = newfilenames


# Change the filenames in the latex file. This blindly changes everything
# in the file.
with open('.'.join([sourcefile, 'tex']), 'r') as f:
    lines = f.readlines()

with open('manuscript.tex', 'w') as f:
    for line in lines:
        for pathname, filename in zip(pathnames, filenames):
            if pathname in line:
                print('Changing: %s' % line)
                line = line.replace(pathname, filename)
                print('to      : %s' % line)
                print('=' * 80)
        f.write(line)
