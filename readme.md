This repository is a template for writing manuscripts in our research group.

Start by creating a "Fork" of this repository; this copies all of the files in this repository into a new repository associated with your bitbucket account. (You should find a button for "fork" on the left pane of this bitbucket window.) Give your repository the name of the manuscript; e.g., "Fairydust2013". Important: Check "This is a private repository."

Next, clone your new manuscript to your machine. From your new repository, clicking the "Clone" button and paste the `git clone ...` code as a command at your terminal / git client.

Finally, make a copy of v0.tex into v1-XXX.tex, where, XXX are your initials, and write away!