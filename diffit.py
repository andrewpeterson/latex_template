#!/usr/bin/env python

import os

oldfile = "v1.tex"
newfile = "v2.tex"

oldbase = os.path.splitext(oldfile)[0]
newbase = os.path.splitext(newfile)[0]
diffbase = 'diff_%s' % newbase

os.system('latexdiff %s %s > %s.tex' % (oldfile, newfile, diffbase))
os.system('pdflatex %s' % diffbase)
os.system('bibtex %s' % diffbase)
os.system('pdflatex %s' % diffbase)
os.system('pdflatex %s' % diffbase)
